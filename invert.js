function invert(obj) {
    if (!obj) return [];
    const Keys = [];
    const Values = [];
    for (let key in obj){
        Keys.push(key);
        Values.push(obj[key]);
    }

    const newObj = {};
    for (i=0; i<Keys.length; i++){
        newObj[Values[i]] = Keys[i];
    }
    return newObj;
}

module.exports = invert;