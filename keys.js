const keys = (obj)=>{
    if (!obj) return [];

    let Keys = [];
    for (let key in obj){
        Keys.push(key)
    }
    return Keys
}

module.exports = keys;