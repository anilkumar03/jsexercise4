const mapObject = (obj,cb,exchange) =>{
    if (!obj) return [];
    
    let Keys = []
    for (let key in obj){
        Keys.push(key)
    }
    for (i=0; i<Keys.length; i++){
        obj[Keys[i]] = cb(i,exchange)
    }
    return obj
}

module.exports = mapObject;