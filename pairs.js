function pairs(obj) {
    if (!obj) return [];
    
    const Pairs = [];

    for (let key in obj){
        Pairs.push( [key,obj[key]] );
    }
    return Pairs;
}

module.exports = pairs